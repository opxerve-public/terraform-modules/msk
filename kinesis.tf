resource "aws_s3_bucket" "bucket" {
  bucket = "${var.project}-${var.env}-msk-broker-logs"

  tags = {
    Name        = "${var.project}-${var.env}-msk-broker-logs"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = var.component
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.bucket.id
  acl    = "private"
}

resource "aws_iam_role" "firehose_role" {
  name = "${var.project}-${var.env}-firehose-role"

  assume_role_policy = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "firehose.amazonaws.com"
    },
    "Effect": "Allow",
    "Sid": ""
  }
  ]
}
EOF
}

resource "aws_kinesis_firehose_delivery_stream" "firehose_stream" {
  name        = "${var.project}-${var.env}-firehose-msk-broker-logs-steam"
  destination = "s3"

  s3_configuration {
    role_arn   = aws_iam_role.firehose_role.arn
    bucket_arn = aws_s3_bucket.bucket.arn
  }

  lifecycle {
    ignore_changes = [
      tags["LogDeliveryEnabled"],
    ]
  }

  tags = {
    Name        = "${var.project}-${var.env}-firehose-msk-broker-logs-steam"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = var.component
    LogDeliveryEnabled = "placeholder"
  }
}
