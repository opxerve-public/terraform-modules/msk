resource "aws_secretsmanager_secret" "msk" {
  name          = "AmazonMSK_${var.kafka.cluster_name}-secret"
  description   = "For store ${var.kafka.cluster_name} username/password"
  kms_key_id    = aws_kms_key.key.id
}

resource "aws_secretsmanager_secret_version" "secret" {
  secret_id     = aws_secretsmanager_secret.msk.id
  secret_string = var.secret_string
}

resource "aws_msk_scram_secret_association" "kafka_secret" {
  cluster_arn     = aws_msk_cluster.kafka.arn
  secret_arn_list = [aws_secretsmanager_secret.msk.arn]

  depends_on = [aws_secretsmanager_secret_version.secret]
}
