#############################
# Cloud Watch Policy
#############################
resource "aws_cloudwatch_log_group" "loggroup" {
  name              = "${var.project}-${var.env}-msk-loggroup"
  retention_in_days = var.kafka.data_retention

  tags = {
    Name        = "${var.project}-${var.env}-msk-loggroup"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = var.component
  }
}
