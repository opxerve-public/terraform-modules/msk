variable "project" {
    description = "Project name for this resource"
    type        = string
}
variable "component" {
    description = "Component name for this resource"
    type        = string
}
variable "env" {
    description = "Environment name for this resource"
    type        = string
}
variable "secret_string" {
    description = "Secret string for username/password Kafka"
    type        = string
}
variable "kafka" {
    description = "Kafka option"
    type = object({
        cluster_name   = string,
        version        = string,
        broker_nodes   = number,
        instance_type  = string,
        volume_size    = number,
        data_retention = number,
    })
}
variable "sg" {
    description = "Security group rules"
    type = list(object({
        port        = string,
        allow_cidr  = list(string),
        allow_sg    = list(string),
        description = string,
        protocol    = string
    }))
}
variable "sg_vpc_id" {
    description = "VPC id"
    type        = string
}
variable "subnet_ids" {
    description = "List of subnet ids"
    type        = list(string)
}
variable "kafka_conf" {
    description = "Kafka config"
    type = object({
        min_insync_replicas = number
    })
}
