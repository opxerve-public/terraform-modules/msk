resource "aws_kms_key" "key" {
  description             = "${var.project}-${var.env}-kms-key"
}

resource "aws_kms_alias" "key-alias" {
  name          = "alias/${var.project}-${var.env}-msk-key"
  target_key_id = aws_kms_key.key.key_id
}
