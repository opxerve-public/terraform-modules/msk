resource "aws_msk_cluster" "kafka" {
  cluster_name           = var.kafka.cluster_name
  kafka_version          = var.kafka.version
  number_of_broker_nodes = var.kafka.broker_nodes

  broker_node_group_info {
    instance_type   = var.kafka.instance_type
    ebs_volume_size = var.kafka.volume_size
    client_subnets  = var.subnet_ids
    security_groups = [aws_security_group.sg.id]
  }

  configuration_info {
    arn      = aws_msk_configuration.cluster.arn
    revision = 1
  }

  encryption_info {
    encryption_in_transit {
      client_broker = "TLS_PLAINTEXT"
      in_cluster = true
    }
  }

  lifecycle {
    ignore_changes = [client_authentication["sasl"],
                      client_authentication["tls"]]
  }

  client_authentication {
    sasl {
      scram = true
    }
    unauthenticated = true
  }

  enhanced_monitoring = "PER_BROKER"

  open_monitoring {
    prometheus {
      jmx_exporter {
        enabled_in_broker = true
      }
      node_exporter {
        enabled_in_broker = true
      }
    }
  }

  logging_info {
    broker_logs {
      cloudwatch_logs {
        enabled   = true
        log_group = aws_cloudwatch_log_group.loggroup.name
      }
      firehose {
        enabled         = true
        delivery_stream = aws_kinesis_firehose_delivery_stream.firehose_stream.name
      }
      s3 {
        enabled = true
        bucket  = aws_s3_bucket.bucket.id
        prefix  = "logs/msk-"
      }
    }
  }

  tags = {
    Name        = var.kafka.cluster_name
    project     = var.project
    environment = var.env
    terraform   = true
    component   = var.component
  }
}

