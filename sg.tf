#############################
# Security Group
#############################
resource "aws_security_group" "sg" {
  name        = "${var.project}-${var.env}-msk-sg"
  description = "${var.project}-${var.env}-msk-sg"
  vpc_id      = var.sg_vpc_id

  dynamic "ingress" {
    for_each = [for rule in var.sg: {
      from_port   = rule.port
      to_port     = rule.port
      protocol    = rule.protocol
      cidr_blocks = rule.allow_cidr
      description = rule.description
      security_groups = rule.allow_sg
    }]

    content {
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
      description = ingress.value.description
      security_groups = ingress.value.security_groups
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "${var.project}-${var.env}-msk-sg"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = var.component
  }
}
