resource "aws_msk_configuration" "cluster" {
  name           = "${var.project}-${var.env}-config"
  kafka_versions = [var.kafka.version]

  server_properties = <<PROPERTIES
auto.create.topics.enable = false
delete.topic.enable = true
log.cleaner.delete.retention.ms = 86400000
log.retention.hours = 24
min.insync.replicas = ${var.kafka_conf.min_insync_replicas}
PROPERTIES
}
